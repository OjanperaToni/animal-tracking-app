﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour {

    public GameObject homePanel;
    public GameObject taskBar;
    public GameObject loginPanel;
    public GameObject[] panels;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            
            if (loginPanel.activeSelf == false)
            {
                Debug.Log("escape key pressed");
                foreach (var panel in panels)
                {
                    if (panel == homePanel)
                    { panel.SetActive(true); }
                    else
                    { panel.SetActive(false); }

                }
                taskBar.SetActive(true);
            }
        }
	}
}

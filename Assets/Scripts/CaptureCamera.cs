﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CaptureCamera : MonoBehaviour {

    public GameObject sprite;

    private WebCamTexture camCapture;

	// Use this for initialization
	void Start () {
        camCapture = new WebCamTexture();
        Debug.Log(camCapture.deviceName);
        GetComponent<RawImage>().texture = camCapture;
        camCapture.Play();
    }

    // Update is called once per frame
    void Update () {
	}

    public void TakePicture()
    {
        camCapture.Pause();
        Texture img = GetComponent<RawImage>().texture;
        Texture2D ss = new Texture2D(img.width, img.height);
        Graphics.CopyTexture(img, 0, 0, ss, 0, 0);
        Sprite s = Sprite.Create(ss, new Rect(0, 0, img.width, img.height), new Vector2(0.5f, 0.5f));
        sprite.SetActive(true);
        sprite.GetComponent<Image>().sprite = s;
        this.gameObject.SetActive(false);
    }

    void OnEnable()
    {
        camCapture.Play();
        sprite.SetActive(false);
    }
}

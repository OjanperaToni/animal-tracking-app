﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarkerManager : MonoBehaviour {
    public GameObject marker;
    public bool tracking = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void AddMarker()
    {
        if (tracking)
        {
            GameObject newMarker = Instantiate(marker);
            newMarker.transform.parent = gameObject.transform;
            newMarker.transform.localPosition = new Vector3(Random.Range(-370f, 370f), Random.Range(-225f, 225f), 0);
        }
    }
    public void TrackStateTrue()
    {
        tracking = true;
    }

    public void TrackStateFalse()
    {
        tracking = false;
    }
}

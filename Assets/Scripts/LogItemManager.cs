﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LogItemManager : MonoBehaviour {

    public RawImage image;
    public Image image2;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void UpdateImage(RawImage newImage)
    {
        image.texture = newImage.texture;
    }
    public void UpdateImg(Image img)
    {
        image2.sprite = img.sprite;
    }
}
